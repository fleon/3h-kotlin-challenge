package com.example.myapplication

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View

class SimpleBoardView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0) :
    View(context, attrs, defStyleAttr, defStyleRes) {

    // Board display parameters
    private var w = 0
    private var h = 0
    private var interval = 0
    private var padding = 5f

    // Paints
    private val paintEmpty = Paint()
        .apply { color = Color.argb(40,0,0,0) }
        .apply { style = Paint.Style.FILL }

    private val paintPlayer = Paint()
        .apply { color = resources.getColor(R.color.colorAccent) }
        .apply { style = Paint.Style.FILL }

    private val paintPossible = Paint()
        .apply { color = Color.argb(80,0,0,255) }
        .apply { style = Paint.Style.FILL }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val desiredWidth = suggestedMinimumWidth + paddingLeft + paddingRight
        w = resolveSize(desiredWidth, widthMeasureSpec)
        h = w
        interval = h/BOARD_SIZE
        setMeasuredDimension(w,w)
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        //Background
        canvas.drawARGB(40, 0, 0, 0)

        for (i in 0 until BOARD_SIZE) {
            for (j in 0 until BOARD_SIZE) {
                var x = (i * interval).toFloat()
                var y = (j * interval).toFloat()

                // IMPORTANT A
                //Draw Empty/Player tiles
                when (MyGame.gameBoard[i][j]){
                    MyTiles.EMPTY.id -> canvas.drawRect(x + padding, y + padding, x + interval - padding, y + interval - padding, paintEmpty)
                    MyTiles.PLAYER.id -> canvas.drawRect(x + padding, y + padding, x + interval - padding, y + interval - padding, paintPlayer)
                }

                //Draw Available moves tiles
                when (MyGame.availableMovesBoard[i][j]) {
                    AvailableTiles.AVAILABLE.id -> {
                        canvas.drawCircle(x + interval / 2, y + interval / 2, (interval / 4).toFloat(), paintPossible)
                    }
                }
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val pointerIndex = event.actionIndex
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_POINTER_DOWN -> return true
            MotionEvent.ACTION_UP,
            MotionEvent.ACTION_POINTER_UP -> {

                val x = event.getX(pointerIndex).toInt()/interval
                val y = event.getY(pointerIndex).toInt()/interval

                Log.d("onTouchEvent","RECEIVED CLICK")
                Log.d("onTouchEvent", "x: $x")
                Log.d("onTouchEvent", "y: $y")

                // IMPORTANT B
                MyGame.onClick(Vector2D(x,y))

                invalidate()
                return true
            }
        }
        return super.onTouchEvent(event)
    }

}