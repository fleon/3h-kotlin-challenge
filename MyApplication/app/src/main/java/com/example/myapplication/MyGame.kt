package com.example.myapplication

enum class MyState {
    DEFAULT,
    PAWN_CLICKED
}

enum class MyTiles(val id : Int) {
    EMPTY(0),
    PLAYER(1),
}

enum class AvailableTiles(val id : Int) {
    NOT_AVAILABLE(0),
    AVAILABLE(1),
}

class Vector2D {
    var x : Int
    var y : Int

    constructor(x: Int, y: Int){
        this.x = x
        this.y = y
    }
}

const val BOARD_SIZE = 10
const val INITIAL_POS_X = 4
const val INITIAL_POS_Y = 3

object MyGame {

    private var currentState : MyState = MyState.DEFAULT

    var gameBoard = Array(BOARD_SIZE) {IntArray(BOARD_SIZE)}
        private set
    var availableMovesBoard = Array(BOARD_SIZE) {IntArray(BOARD_SIZE)}
        private set

    init {
        gameBoard[INITIAL_POS_X][INITIAL_POS_Y] = MyTiles.PLAYER.id
    }

    fun onClick(pos : Vector2D) {

        when (currentState) {

            MyState.DEFAULT -> managePlayer(pos)

            MyState.PAWN_CLICKED -> manageMovement(pos)

        }
    }

    private fun managePlayer(pos : Vector2D){
        // If tile clicked is a player
        if (this.gameBoard[pos.x][pos.y] == MyTiles.PLAYER.id){

            // Change state to "clicked"
            this.currentState = MyState.PAWN_CLICKED

            // Add possible moves to the "possible" board
            for (i in 0 until BOARD_SIZE){
                this.availableMovesBoard[i][pos.y] = AvailableTiles.AVAILABLE.id
                this.availableMovesBoard[pos.x][i] = AvailableTiles.AVAILABLE.id
            }
            // Remove player spot
            this.availableMovesBoard[pos.x][pos.y] = AvailableTiles.NOT_AVAILABLE.id
        }
    }

    private fun manageMovement(pos : Vector2D) {
        // If tile clicked is a possible tile
        if (this.availableMovesBoard[pos.x][pos.y] == AvailableTiles.AVAILABLE.id) {

            // Move player
            // TODO: Find better. This is a brutal way to move player
            this.gameBoard = Array(BOARD_SIZE) { IntArray(BOARD_SIZE) }
            this.gameBoard[pos.x][pos.y] = MyTiles.PLAYER.id

            // Reset availableMovesBoard
            this.availableMovesBoard = Array(BOARD_SIZE) {IntArray(BOARD_SIZE)}

            // Change state to 'default'
            this.currentState = MyState.DEFAULT
        }
    }

}